import { Component } from '@angular/core';
import * as Excel from "exceljs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'ExcelExport';

  exportExcel(){
    const options = {
      filename: './streamed-workbook.xlsx',
      useStyles: true,
      useSharedStrings: true
    };
    let workbook = new Excel.stream.xlsx.WorkbookWriter(options);
    let worksheet = workbook.addWorksheet("Sheet1");

    let header = worksheet.addRow(['h1','h2','h3','h4']).commit();
    let row = worksheet.addRow(['data1','data2','data3','data4']).commit();
    
    workbook.commit().then(()=>
      console.log("file exported successfully")
    )
  }
}
